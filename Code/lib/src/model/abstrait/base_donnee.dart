import 'package:flutter_html/flutter_html.dart';

/// La base des données reliées à un personnage.
abstract class BaseDonnee {
  //region Propriétés
  /// Le nom principal de l'élément.
  final String nom;

  /// La description de l'élément.
  final String? description;

  /// Les traductions de l'élément (FR).
  final Translations? translations;
  //endregion

  //region Constructeur
  /// Constructeur
  const BaseDonnee({required this.nom, this.description, this.translations});
  //endregion

  //region Méthodes
  /// Récupère le nom de l'élément.
  String getNom() {
    if (translations == null) {
      return nom;
    } else {
      return translations?.fr?.nom ?? "";
    }
  }

  /// Retourne une description abrégée de l'élément
  Html? getWidgetCourteDescription() {
    int indexFinParagraphe;
    Html html;
    if (translations != null) {
      indexFinParagraphe =
          translations?.fr?.description?.indexOf("</p>", 2) as int;
      return Html(
          data:
              translations?.fr?.description?.substring(0, indexFinParagraphe));
    } else {
      indexFinParagraphe = description?.indexOf("</p>", 2) as int;
      return Html(data: description?.substring(0, indexFinParagraphe));
    }
  }
  //endregion
}

/// Les différentes traductions de l'élément (Uniquement FR)
class Translations {
  Fr? fr;

  Translations({this.fr});

  Translations.fromJson(Map<String, dynamic> json) {
    fr = json['fr'] != null ? Fr?.fromJson(json['fr']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['fr'] = fr!.toJson();
    return data;
  }
}

/// La traduction en français d'un élément
class Fr {
  String? status;
  String? nom;
  String? description;

  Fr({this.status, this.nom, this.description});

  Fr.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    nom = json['name'];
    description = json['description'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['status'] = status;
    data['name'] = nom;
    data['description'] = description;
    return data;
  }
}
