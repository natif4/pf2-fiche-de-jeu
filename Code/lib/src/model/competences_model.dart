import 'package:fiche_de_jeu/src/model/attributs_model.dart';
import 'package:fiche_de_jeu/src/model/niveau_maitrise_model.dart';

enum Competences {
  //region Énumérations
  acrobaties(
      fr: 'Acrobaties',
      en: 'Acrobatics',
      attributCorrespondant: Attributs.dex,
      estAffecteParArmure: true),
  arcane(fr: 'Arcane', en: 'Arcana', attributCorrespondant: Attributs.int),
  athletisme(
      fr: 'Athlétisme',
      en: 'Athetics',
      attributCorrespondant: Attributs.str,
      estAffecteParArmure: true),
  artisanat(
      fr: 'Artisanat', en: 'Crafting', attributCorrespondant: Attributs.int),
  duperie(fr: 'Duperie', en: 'Deception', attributCorrespondant: Attributs.cha),
  diplomatie(
      fr: 'Diplomatie', en: 'Diplomacy', attributCorrespondant: Attributs.cha),
  intimidation(
      fr: 'Intimidation',
      en: 'Intimidation',
      attributCorrespondant: Attributs.cha),
  medecine(
      fr: 'Médecine', en: 'Medicine', attributCorrespondant: Attributs.wis),
  nature(fr: 'Nature', en: 'Nature', attributCorrespondant: Attributs.wis),
  occultisme(
      fr: 'Occultisme', en: 'Occultism', attributCorrespondant: Attributs.int),
  representation(
      fr: 'Représentation',
      en: 'Performance',
      attributCorrespondant: Attributs.cha),
  religion(
      fr: 'Religion', en: 'Religion', attributCorrespondant: Attributs.wis),
  societe(fr: 'Société', en: 'Society', attributCorrespondant: Attributs.int),
  discretion(
      fr: 'Discrétion',
      en: 'Stealth',
      attributCorrespondant: Attributs.dex,
      estAffecteParArmure: true),
  survie(fr: 'Survie', en: 'Survival', attributCorrespondant: Attributs.wis),
  vol(
      fr: 'Vol',
      en: 'Thievery',
      attributCorrespondant: Attributs.dex,
      estAffecteParArmure: true);
  //endregion

  //region Propriétés
  /// Nom en français
  final String fr;

  /// Nom en anglais
  final String en;

  /// L'attribut affectant la compétence
  final Attributs attributCorrespondant;

  /// L'armure affecte la compétence
  final bool estAffecteParArmure;
  //endregion

  //region Constructeur
  const Competences(
      {required this.fr,
      required this.en,
      required this.attributCorrespondant,
      this.estAffecteParArmure = false});
  //endregion
}

/// Le regroupement des comptétences d'un personnage.
class CompetencesPersonnage {
  late Map<Competences, Maitrises?> _competences;

  CompetencesPersonnage(Map<Competences, Maitrises?> competences) {
    _competences.addAll(competences);
  }

  /// Retourne le niveau de maîtrise d'une compétence
  Maitrises? obtenirNiveauMaitrise(Competences competence) {
    return _competences.putIfAbsent(competence, () => null);
  }

  /// Ajoute ou modifie le niveau de maitrise d'une compétence.
  void ajouterOuModifierCompetence(Competences competence, Maitrises maitrise) {
    _competences.putIfAbsent(competence, () => maitrise);
  }
}
