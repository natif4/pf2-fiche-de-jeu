import 'package:fiche_de_jeu/src/model/abstrait/base_donnee.dart';

/// L'historique (le passé) d'un personnage.
class Historique extends BaseDonnee {
  //region Propriétés
  /// L'id de l'historique
  late String id;
  //endregion

  //region Constructeurs
  /// Constructeur de base.
  Historique(
      {required this.id,
      required String nom,
      String? description,
      Translations? translations})
      : super(nom: nom, description: description, translations: translations);

  /// Constructeur avec JSON.
  Historique.fromJson(Map<String, dynamic> json)
      : super(
            nom: json['name'],
            description: json['description'],
            translations: json['translations'] != null
                ? Translations?.fromJson(json['translations'])
                : null) {
    id = json['_id'];
  }
  //endregion
}
