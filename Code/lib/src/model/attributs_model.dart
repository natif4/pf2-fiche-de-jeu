enum Attributs {
  str(nomAttributFR: 'Force', acronymeFR: 'FOR', nomAttributEN: 'Strength', acronymeEN: 'STR'),
  dex(nomAttributFR: 'Dextérité', acronymeFR: 'DEX', nomAttributEN: 'Dexterity', acronymeEN: 'DEX'),
  con(nomAttributFR: 'Constitution', acronymeFR: 'CON', nomAttributEN: 'Constitution', acronymeEN: 'CON'),
  int(nomAttributFR: 'Intelligence', acronymeFR: 'INT', nomAttributEN: 'Intelligence', acronymeEN: 'INT'),
  wis(nomAttributFR: 'Sagesse', acronymeFR: 'SAG', nomAttributEN: 'Wisdom', acronymeEN: 'WIS'),
  cha(nomAttributFR: 'Charisme', acronymeFR: 'CHA', nomAttributEN: 'Charisma', acronymeEN: 'CHA');


  const Attributs({
  required this.nomAttributFR,
required this.acronymeFR,
required this.nomAttributEN,
required this.acronymeEN
  });

  final String nomAttributFR;
  final String acronymeFR;
  final String nomAttributEN;
  final String acronymeEN;
}

class AttributsPersonnage {
  late Map<Attributs, ValeurAttribut> attributs;

  AttributsPersonnage(int force, int dex, int con, int intel, int sag, int cha) {
    attributs = {
      Attributs.str : ValeurAttribut(valeur:force),
      Attributs.dex : ValeurAttribut(valeur:dex),
      Attributs.con : ValeurAttribut(valeur:con),
      Attributs.int : ValeurAttribut(valeur:intel),
      Attributs.wis : ValeurAttribut(valeur:sag),
      Attributs.cha : ValeurAttribut(valeur:cha)
    };
  }
}

class ValeurAttribut {
  late int valeur;
  late int modificateur;

  ValeurAttribut({required this.valeur});

  int getModificateur() {
    return ((valeur - 10) / 2).round();
  }
}
