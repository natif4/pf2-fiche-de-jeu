import 'package:fiche_de_jeu/src/model/abstrait/base_donnee.dart';
import 'package:flutter_html/flutter_html.dart';

class Ascendance extends BaseDonnee {
  late String id;
  List<String?>? additionalLanguages;
  int? hp;
  List<String?>? languages;

  Ascendance(
      {required this.id,
      required String nom,
      String? description,
      this.additionalLanguages,
      this.hp,
      this.languages,
      Translations? translations})
      : super(nom: nom, description: description, translations: translations);

  Ascendance.fromJson(Map<String, dynamic> json)
      : super(
            nom: json['name'],
            description: json['description'],
            translations: json['translations'] != null
                ? Translations?.fromJson(json['translations'])
                : null) {
    id = json['_id'];
    if (json['additionalLanguages'] != null) {
      additionalLanguages = <String>[];
      json['additionalLanguages'].forEach((v) {
        additionalLanguages!.add(v);
        /*additionalLanguages!.add(.fromJson(v));*/
      });
    }
    hp = json['hp'];
    if (json['languages'] != null) {
      languages = <String>[];
      json['languages'].forEach((v) {
        languages!.add(v);
      });
    }
  }
}
