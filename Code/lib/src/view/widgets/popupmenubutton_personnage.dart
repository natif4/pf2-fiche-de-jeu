import 'package:flutter/material.dart';

/*class PopupMenuWidget extends StatefulWidget {
  final List<String> lstChoix;

  const PopupMenuWidget ({
    super.key,
    required this.lstChoix
  });

  @override
  State<PopupMenuWidget> createState() => _PopupMenuWidgetState();
}

class _PopupMenuWidgetState extends State<PopupMenuWidget> {
  String? choix;
  final VoidCallback onTap;

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton<String>(
      initialValue: _choix,
      onSelected: (String selection) {
        setState(() {
          _choix = selection;
        });
      },
      itemBuilder: (BuildContext context) {
        return PopupMenuEntry((String valeur) {
          widget.lstChoix.map<PopupMenuItem<String>((String valeur) {
    return PopupMenuItem(value: valeur,child: Text(valeur));
    }
    );
        });}
    widget
        }.lstChoix.map<PopupMenuItem<String>>((String valeur) {
          return PopupMenuItem(value: valeur,child: Text(valeur));
        })
      }
    );
  }
}*/

class PopupMenuCreation extends StatefulWidget {
  final List<String> choix;
  final InputDecoration? decoration;
  const PopupMenuCreation({super.key, required this.choix, this.decoration});

  @override
  _PopupMenuCreation createState() => new _PopupMenuCreation();
}

class _PopupMenuCreation extends State<PopupMenuCreation> {
  String selection = "";

  void _montrerPopup(BuildContext context) {
    showMenu<String>(
        context: context,
        position: const RelativeRect.fromLTRB(0, 0, 0, 0),
        items: widget.choix.map((String objet) {
          return PopupMenuItem<String>(
            value: objet,
            child: ListTile(
                title: Text(objet),
                onTap: () {
                  setState(() {
                    selection = objet;
                  });
                }),
          );
        }).toList());
  }

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
        onPressed: () => _montrerPopup(context), child: Text(selection));
/*return PopupMenuButton(
          initialValue: widget.choixxxx[0],
          onSelected: (value) {
            setState(() {
              selection = value;
            });
          },
          itemBuilder: (BuildContext context) => widget.choixxxx
              .map((String objet) =>
              PopupMenuItem<String>(value: objet, child: Text(objet)))
              .toList(),
          child: TextField(

            decoration: widget.decoration,
            enabled: false,
          ),
        ))
      ],
    );*/

    /*return PopupMenuButton(
      offset: Offset(context.,0),
      initialValue: widget.choixxxx[0],
      onSelected: (value) {
        setState(() {
          selection = value;
        });
      },
      itemBuilder: (BuildContext context) => widget.choixxxx
          .map((String objet) =>
              PopupMenuItem<String>(value: objet, child: Text(objet)))
          .toList(),
      child: TextField(

        decoration: widget.decoration,
        enabled: false,
      ),
    );*/
  }

  /*static PopupMenuButton<String> creerPopup(List<PopupMenuCreation> lstObjets) {
    return PopupMenuButton(
      onSelected: (valeur) {
        lstObjets.firstWhere((objet) => objet.titre == valeur).apresAppuie();
      },
      itemBuilder: (context) => lstObjets.map((objet) => PopupMenuItem<String>(
        value: objet.titre,
        child: Text(objet.titre)
      )).toList()
    );
  }*/
}
