import 'package:flutter/material.dart';

class LabelTitre extends StatelessWidget {
  final String texteAAfficher;
  const LabelTitre({super.key, required this.texteAAfficher});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 25,
      width: double.maxFinite,
      decoration: const BoxDecoration(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(10), topRight: Radius.circular(10)),
          gradient: LinearGradient(colors: [
            Color.fromRGBO(105, 20, 14, 1),
            Color.fromRGBO(105, 20, 14, 0.74),
            Color.fromRGBO(105, 20, 14, 1),
          ], begin: Alignment.topCenter, end: Alignment.bottomCenter)),
      alignment: Alignment.center,
      child: Text(texteAAfficher,
          style: const TextStyle(
              fontWeight: FontWeight.w600,
              color: Colors.white70,
              fontSize: 18)),
    );
  }
}
