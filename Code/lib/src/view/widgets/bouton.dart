import 'package:flutter/material.dart';
export 'package:fiche_de_jeu/src/view/pages/main.dart';

class BoutonWidget extends StatelessWidget {
  final ButtonStyle style;
  final String texteAAfficher;
  final Function fonction;

  void onPressedEvenement() {
    fonction();
  }

  const BoutonWidget(
      {Key? key,
      required this.style,
      required this.texteAAfficher,
      required this.fonction})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: style,
      onPressed: onPressedEvenement,
      child: Text(
        texteAAfficher,
        style: const TextStyle(fontSize: 15),
      ),
    );
  }
}
