import 'package:fiche_de_jeu/src/model/abstrait/base_donnee.dart';
import 'package:fiche_de_jeu/src/model/ascendance_model.dart';
import 'package:flutter/material.dart';

/// Un bouton affichant un menu dont les éléments se développent et sont sélectionnables.
class BoutonMenuSelection<T extends BaseDonnee> extends StatefulWidget {
  //region Propriétés
  /// Le titre du menu.
  final String titre;

  /// La liste des choix possibles.
  final List<T> lstChoix;

  /// La valeur initiale à afficher sur le bouton.
  final T? valeurInitiale;
  //endregion

  //region Constructeur
  /// Constructeur
  ///
  /// Nécéssite le [titre] du menu à afficher ainsi que [lstChoix].
  const BoutonMenuSelection(
      {super.key,
      required this.titre,
      required this.lstChoix,
      this.valeurInitiale});

  @override
  State<BoutonMenuSelection<T>> createState() => _BoutonMenuSelection();
  //endregion
}

/// L'état de [BoutonMenuSelection]
class _BoutonMenuSelection<T extends BaseDonnee>
    extends State<BoutonMenuSelection<T>> {
  //region Propriétés
  T? _choix;
  String _texteChoix = "";
  late Widget menu;
  //endregion

  //region Méthodes privées
  /// Affiche le menu de sélection et retourne le [_choix] sélectionné.
  Future<void> _afficherMenuEtRetourneSelection(BuildContext context) async {
    menu = _MenuSelection(
        titre: widget.titre, choix: _choix!, lstDonnees: widget.lstChoix);

    final resultat = await Navigator.push(
        context, MaterialPageRoute(builder: (context) => menu));
    if (!mounted) return;
    _choix = resultat;
    _texteChoix = _choix?.getNom() ?? "";
    setState(() {});
  }
  //endregion

  //region WidgetBuild
  /// Création du Widget
  @override
  Widget build(BuildContext context) {
    if (_choix == null) {
      if (widget.valeurInitiale != null) {
        _choix = widget.valeurInitiale;
      } else {
        _choix = widget.lstChoix[0];
      }
      _texteChoix = _choix!.getNom();
    }

    return ElevatedButton(
        onPressed: () => _afficherMenuEtRetourneSelection(context),
        child: Text(_texteChoix));
  }
  //endregion
}

/// Un menu permettant la sélection d'un élément qui est développable.
class _MenuSelection extends StatefulWidget {
  //region Propriétés
  final String titre;
  final BaseDonnee choix;
  final List<BaseDonnee> lstDonnees;
  //endregion

  //region Constructeur
  const _MenuSelection(
      {required this.titre, required this.choix, required this.lstDonnees});

  @override
  State<_MenuSelection> createState() => _MenuSelectionState();
  //endregion
}

/// État de [_MenuSelection].
class _MenuSelectionState extends State<_MenuSelection> {
  //region Propriétés
  static const EdgeInsets _paddingElement = EdgeInsets.all(15);

  /// L'index du choix en cours.
  int _indexChoix = -1;

  /// Le nouveau choix de l'utilisateur.
  BaseDonnee? nouvChoix;

  /// La liste des éléments possibles à sélectionner
  late List<_ExpansionTileInfo> lstElements;
  //endregion

  //region Méthodes privées
  List<_ExpansionTileInfo> genererElements<T extends BaseDonnee>() {
    return List<_ExpansionTileInfo>.generate(widget.lstDonnees.length, (index) {
      return _ExpansionTileInfo(
          index: index,
          titre: widget.lstDonnees[index].getNom(),
          contenu: ListTile(
              key: Key(index.toString()),
              title: afficherContenu(widget.lstDonnees[index])));
    });
  }

  /// Prépare l'affichage des éléments d'une ascendance.
  Column afficherContenu<T extends BaseDonnee>(T donnee) {
    Widget? html = donnee.getWidgetCourteDescription();
    List<Widget> lstWidgets = <Widget>[];

    switch (T) {
      case Ascendance:
        lstWidgets.add(Row(children: [
          const Text("HP : "),
          Text((donnee as Ascendance).hp.toString())
        ]));
        break;
      default:
        break;
    }

    if (html != null) {
      lstWidgets.add(Column(children: [const Text("Description : "), html]));
    }
    return Column(children: lstWidgets);
  }

  /// Fabrique et retourne la liste développable.
  Widget obtenirListeSelections() {
    return ExpansionPanelList(
      expansionCallback: (int index, bool isExpanded) {
        setState(() {
          lstElements[index].estOuvert = !isExpanded;
          if (isExpanded) {
            _indexChoix = -1;
          } else {
            nouvChoix = widget.lstDonnees[index];
            _indexChoix = index;
          }
        });
      },
      children:
          lstElements.map<ExpansionPanel>((_ExpansionTileInfo expansionTile) {
        return ExpansionPanel(
            headerBuilder: (BuildContext context, bool estOuvert) {
              return ListTile(
                title: Text(expansionTile.titre),
                onTap: () {
                  setState(() {
                    if (expansionTile.estOuvert) {
                      expansionTile.estOuvert = false;
                    } else {
                      if (_indexChoix != -1 &&
                          lstElements[_indexChoix].estOuvert) {
                        lstElements[_indexChoix].estOuvert = false;
                      }
                      expansionTile.estOuvert = true;
                      nouvChoix = widget.lstDonnees[expansionTile.index];
                      _indexChoix = expansionTile.index;
                    }
                  });
                },
              );
            },
            body: expansionTile.contenu,
            isExpanded: expansionTile.estOuvert);
      }).toList(),
    );
  }
  //endregion

  //region WidgetBuild
  /// Initialisation du Widget
  @override
  void initState() {
    super.initState();
    lstElements = genererElements();
  }

  /// Construction du Widget
  @override
  Widget build(BuildContext context) {
    return Dialog(
      insetPadding: _paddingElement,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: Scaffold(
        appBar: AppBar(title: Text(widget.titre)),
        body: Container(
            constraints: BoxConstraints(
                maxHeight: MediaQuery.of(context).size.height - 10,
                maxWidth: MediaQuery.of(context).size.height - 10,
                minHeight: 400,
                minWidth: 200),
            child: Column(
              children: [
                Expanded(
                  child: Scrollbar(
                      thumbVisibility: true,
                      child: SingleChildScrollView(
                          child: obtenirListeSelections())),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    OutlinedButton(
                        onPressed: () {
                          Navigator.pop(context, nouvChoix);
                        },
                        child: const Text("Confirmer")),
                    OutlinedButton(
                        onPressed: () => Navigator.pop(context),
                        child: const Text("Annuler"))
                  ],
                )
              ],
            )),
      ),
    );
  }
  //endregion
}

/// Un élément à afficher dans [_MenuSelection].
class _ExpansionTileInfo {
  //region Propriétés
  /// Titre principal à afficher.
  final String titre;

  /// Index de l'[_ExpansionTileInfo].
  final int index;

  /// Contenu de l'[_ExpansionTileInfo].
  ListTile contenu;

  /// L'[_ExpansionTileInfo] est développé.
  bool estOuvert = false;
  //endregion

  //region Constructeur
  _ExpansionTileInfo({
    required this.titre,
    required this.index,
    required this.contenu,
  });
  //endregion
}

// Ces méthodes restent au cas ou celle d'au dessus ne suffirait pas au reste du travail.

/*
class _Menu extends StatefulWidget {
  final String titre;
  final String choix;
  final List<String> lstChoix;

  /// Constructeur
  ///
  /// Nécéssite le [titre] du menu à afficher. Le [choix] à envoyer est
  /// la valeur choisie précédemment.
  const _Menu({
    super.key,
    required this.titre,
    required this.choix,
    required this.lstChoix,
  });

  @override
  _MenuChoix createState() => _MenuChoix();
}

class _MenuChoix extends State<_Menu> {
  static const EdgeInsets _paddingElement = EdgeInsets.all(15);
  int _indexChoix = 0;
  late String nouvChoix = "";

  void verifierChoix() {
    if (nouvChoix == "") {
      _indexChoix = widget.lstChoix.indexOf(widget.choix);
    } else {
      _indexChoix = widget.lstChoix.indexOf(nouvChoix);
    }
  }

  @override
  Widget build(BuildContext context) {
    verifierChoix();
    return Dialog(
      insetPadding: _paddingElement,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: Scaffold(
        appBar: AppBar(title: Text(widget.titre)),
        body: Container(
            constraints: BoxConstraints(
                maxHeight: MediaQuery.of(context).size.height - 10,
                maxWidth: MediaQuery.of(context).size.height - 10,
                minHeight: 400,
                minWidth: 200),
            child: Column(
              children: [
                Expanded(
                    child: ListView.builder(
                  itemCount: widget.lstChoix.length,
                  prototypeItem: ListTile(
                    title: Text(widget.lstChoix.first),
                  ),
                  itemBuilder: (BuildContext context, int index) {
                    return ListTile(
                      selected: index == _indexChoix,
                      onTap: () {
                        setState(() {
                          _indexChoix = index;
                          nouvChoix = widget.lstChoix[index];
                        });
                      },
                      title: Text(widget.lstChoix[index]),
                    );
                  },
                )),
                Row(
                  children: [
                    OutlinedButton(
                        onPressed: () {
                          Navigator.pop(context, nouvChoix);
                        },
                        child: const Text("Confirmer")),
                    OutlinedButton(
                        onPressed: () => Navigator.pop(context),
                        child: const Text("Annuler"))
                  ],
                )
              ],
            )),
      ),
    );
  }
}
*/
