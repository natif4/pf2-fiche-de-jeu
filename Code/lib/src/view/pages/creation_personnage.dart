import 'package:fiche_de_jeu/src/model/attributs_model.dart';
import 'package:fiche_de_jeu/src/model/classe_model.dart';
import 'package:fiche_de_jeu/src/model/historique_model.dart';
import 'package:fiche_de_jeu/src/view/widgets/label_titre.dart';
import 'package:fiche_de_jeu/src/view/widgets/menu_selection.dart';
import 'package:fiche_de_jeu/src/model/ascendance_model.dart';
import 'package:flutter/material.dart';
import 'dart:convert';

import 'package:flutter/services.dart';
export 'package:fiche_de_jeu/src/view/pages/main.dart';

/// Une page servant à la création d'un personnage.
class PageCreation extends StatelessWidget {
  const PageCreation({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Création de personnage",
      home: PageCreationPersonnage(),
    );
  }
}

/// Des options pour la création d'un personnage contenu dans un Widget
class PageCreationPersonnage extends StatefulWidget {
  const PageCreationPersonnage({super.key});

  @override
  State<PageCreationPersonnage> createState() => _PageCreationPersonnage();
}

/// État de [PageCreationPersonnage]
class _PageCreationPersonnage extends State<PageCreationPersonnage> {
  //region Propriétés
  /// Liste des ascendances
  List<Ascendance> _lstAscendances = <Ascendance>[];

  /// Liste des historiques
  List<Historique> _lstHistoriques = <Historique>[];

  /// Liste des classes
  List<Classe> _lstClasses = <Classe>[];

  // Variables pour la taille de l'écran
  late double _hauteurEcran;

  // Reste des variables
  static const SizedBox _espace = SizedBox(height: 20);
  final BoxDecoration _boxDecoration = BoxDecoration(
      borderRadius: const BorderRadius.only(
          bottomLeft: Radius.circular(10), bottomRight: Radius.circular(10)),
      border: Border.all(color: Colors.black));
  static const EdgeInsets _paddingElement = EdgeInsets.all(10);
  final Widget _placeholder = const Text("placeholder");
  //endregion

  //region Méthodes privées
  /// Charge les données en lien avec les ascendances.
  Future<void> _chargerAscendances() async {
    String contenu = await rootBundle.loadString('data/JSON/ancestries.json');
    List<Map<String, dynamic>> ascendancesJSON =
        (json.decode(contenu) as List).cast();
    _lstAscendances = List.generate(ascendancesJSON.length,
        (index) => Ascendance.fromJson(ascendancesJSON[index]));
    _lstAscendances
        .sort((Ascendance a, Ascendance b) => a.nom.compareTo(b.nom));
  }

  /// Charge les données en lien avec les classes.
  Future<void> _chargerClasses() async {
    // Charger les classes
    String contenu = await rootBundle.loadString('data/JSON/classes.json');
    List<Map<String, dynamic>> classesJSON =
        (json.decode(contenu) as List).cast();
    _lstClasses = List.generate(
        classesJSON.length, (index) => Classe.fromJson(classesJSON[index]));
    _lstClasses.sort((Classe a, Classe b) => a.nom.compareTo(b.nom));
  }

  /// Charge les données en lien avec les historiques.
  Future<void> _chargerHistoriques() async {
    // Charger les classes
    String contenu = await rootBundle.loadString('data/JSON/backgrounds.json');
    List<Map<String, dynamic>> historiquesJSON =
        (json.decode(contenu) as List).cast();
    _lstHistoriques = List.generate(historiquesJSON.length,
        (index) => Historique.fromJson(historiquesJSON[index]));
    _lstHistoriques
        .sort((Historique a, Historique b) => a.nom.compareTo(b.nom));
  }

  /// Créer un Widget pour inscrire la valeur d'un attribut.
  Widget definirAttribut(Attributs attribut) {
    return Expanded(
        child: Container(
            decoration: _boxDecoration,
            child: Column(
              children: [
                Text(attribut.acronymeFR),
                TextField(
                  textAlign: TextAlign.center,
                  keyboardType: TextInputType.number,
                  inputFormatters: [
                    FilteringTextInputFormatter.allow(RegExp(r'^([012468]|1[02468]|20)$')),

                  ],
                )
              ],
            )));
  }

  Widget formInformationDeBase() {
    return Column(
      children: [
        const LabelTitre(texteAAfficher: "Nouveau personnage"),
        Container(
          padding: _paddingElement,
          decoration: _boxDecoration,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text('Nom du personnage'),
              TextFormField(
                keyboardType: TextInputType.name,
                decoration: const InputDecoration(
                  hintText: 'Entrez le nom du personnage',
                ),
              )
            ],
          ),
        )
      ],
    );
  }

  /// La section affichant les sélections de base.
  Widget formChoixInitiaux() {
    Widget futureSelectionAscendance = FutureBuilder(
        future: _chargerAscendances(),
        builder: (context, AsyncSnapshot snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(child: CircularProgressIndicator());
          } else if (snapshot.connectionState == ConnectionState.done) {
            return BoutonMenuSelection(
              titre: 'Ascendance',
              lstChoix: _lstAscendances,
            );
          } else {
            return Container();
          }
        });

    Widget futureSelectionClasse = FutureBuilder(
        future: _chargerClasses(),
        builder: (context, AsyncSnapshot snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(child: CircularProgressIndicator());
          } else if (snapshot.connectionState == ConnectionState.done) {
            return BoutonMenuSelection(
              titre: 'Classe',
              lstChoix: _lstClasses,
            );
          } else {
            return Container();
          }
        });

    Widget futureSelectionHistorique = FutureBuilder(
        future: _chargerHistoriques(),
        builder: (context, AsyncSnapshot snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(child: CircularProgressIndicator());
          } else if (snapshot.connectionState == ConnectionState.done) {
            return BoutonMenuSelection(
              titre: 'Historique',
              lstChoix: _lstHistoriques,
            );
          } else {
            return Container();
          }
        });

    return Column(children: [
      const LabelTitre(texteAAfficher: 'Choix initiaux'),
      Container(
          padding: _paddingElement,
          decoration: _boxDecoration,
          child: Column(
            children: [
              Row(
                children: [
                  Expanded(
                      flex: 1,
                      child: Column(
                        children: [
                          const Text(
                            "Ascendance",
                            textAlign: TextAlign.justify,
                          ),
                          Flex(
                            direction: Axis.horizontal,
                            children: [
                              Expanded(
                                child: futureSelectionAscendance,
                              )
                            ],
                          )
                        ],
                      )),
                  const SizedBox(width: 30),
                  Expanded(
                      flex: 1,
                      child: Row(
                        children: [
                          Expanded(
                              flex: 1,
                              child: Column(
                                children: [
                                  const Text(
                                    "Historique",
                                    textAlign: TextAlign.justify,
                                  ),
                                  Flex(
                                    direction: Axis.horizontal,
                                    children: [
                                      Expanded(child: futureSelectionHistorique)
                                    ],
                                  )
                                ],
                              ))
                        ],
                      ))
                ],
              ),
              Column(
                children: [
                  const SizedBox(
                    height: 10,
                  ),
                  const Text(
                    "Classe",
                    textAlign: TextAlign.justify,
                  ),
                  Row(children: [Expanded(child: futureSelectionClasse)])
                ],
              )
            ],
          ))
    ]);
  }

  Widget formAttributsEtCompetences() {
    return Column(children: [
      const LabelTitre(texteAAfficher: 'Attributs et Statistiques'),
      Container(
        padding: _paddingElement,
        decoration: _boxDecoration,
        child: Row(
          children: [
            definirAttribut(Attributs.str),
            definirAttribut(Attributs.dex),
            definirAttribut(Attributs.con),
            definirAttribut(Attributs.int),
            definirAttribut(Attributs.wis),
            definirAttribut(Attributs.cha)
          ],
        ),
      )
    ]);
  }

  //endregion

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      await _chargerAscendances();
      await _chargerClasses();
    });
  }

  @override
  Widget build(BuildContext context) {
    // Variables pour les choix effectués
    _hauteurEcran = MediaQuery.of(context).size.height * 0.1;
    if (_hauteurEcran > 60) {
      _hauteurEcran = 60;
    }

    return Scaffold(
        appBar: AppBar(
          title: const Text(
            "Création d'un personnage",
          ),
          toolbarHeight: _hauteurEcran,
        ),
        body: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height - _hauteurEcran,
            child: Scrollbar(
                thumbVisibility: true,
                thickness: 10,
                radius: const Radius.circular(5),
                scrollbarOrientation: ScrollbarOrientation.right,
                child: SingleChildScrollView(
                  padding: _paddingElement,
                  child: Column(
                    children: [
                      formInformationDeBase(),
                      _espace,
                      formChoixInitiaux(),
                      _espace,
                      formAttributsEtCompetences()
                    ],
                  ),
                ))));
  }
}

class _ChoixAttributs extends StatefulWidget {
  final String choixAscendance;

  const _ChoixAttributs({required this.choixAscendance});

  @override
  State<_ChoixAttributs> createState() => _ChoixAttributsState();
}

class _ChoixAttributsState extends State<_ChoixAttributs> {
  List<bool> lstChoixAttributsAscendance = <bool>[
    false,
    false,
    false,
    false,
    false,
    false
  ];
  late _SelectionAttAscendance _attAscendance;

  _ChoixAttributsState() {
    if (widget.choixAscendance == 'Humain') {
      _attAscendance = _SelectionAttAscendance();
    } else if (widget.choixAscendance == 'Elfe') {
      _attAscendance = _SelectionAttAscendance.avecModifInitial(1, 5, 2);
    } else {
      _attAscendance = _SelectionAttAscendance.avecModifInitial(2, 4, 5);
    }
  }

  void selectionnerAttribut(int index) {
    if (_attAscendance.lstChoixAttributsAscendance[index].estModifiable ==
        true) {
      if (_attAscendance.lstChoixAttributsAscendance[index].estChoisi == true) {
        _attAscendance.lstChoixAttributsAscendance[index].estChoisi == false;
      } else {
        _attAscendance.lstChoixAttributsAscendance[index].estChoisi == true;
      }
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          child: Column(
            children: [
              const Text('Attributs'),
              const SizedBox(height: 15),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  const Text('STR'),
                  const Text('DEX'),
                  const Text('CON'),
                  const Text('INT'),
                  const Text('WIS'),
                  const Text('CHA')
                ],
              ),
              const SizedBox(
                height: 5,
              ),
              const Text('Ascendance'),
              ListView(children: [
                ListTile(
                    selected: _attAscendance
                            .lstChoixAttributsAscendance[0].estChoisi ??
                        false,
                    onTap: () => selectionnerAttribut(0)),
              ]),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [],
              )
            ],
          ),
        )
      ],
    );
  }
}

class _SelectionAttAscendance {
  late int indexChoix;
  late int qteBonusRestant;
  late List<int> lstAttributDeBase;
  late List<_ChoixAttributModel> lstChoixAttributsAscendance;

  _SelectionAttAscendance() {
    lstChoixAttributsAscendance =
        List.generate(6, (index) => _ChoixAttributModel());
    qteBonusRestant = 2;
  }

  _SelectionAttAscendance.avecModifInitial(
      int bonus1, int bonus2, int faiblesse) {
    lstChoixAttributsAscendance = List.generate(
      6,
      (index) {
        if (index == bonus1 || index == bonus2) {
          return _ChoixAttributModel.nonModifiable(true);
        } else if (index == faiblesse) {
          return _ChoixAttributModel.nonModifiable(null);
        } else {
          return _ChoixAttributModel();
        }
      },
    );
    qteBonusRestant = 1;
  }
}

class _ChoixAttributModel {
  bool? estChoisi = false;
  bool estModifiable = true;

  _ChoixAttributModel();
  _ChoixAttributModel.nonModifiable(bool? attributEstChoisi) {
    estChoisi = attributEstChoisi;
    estModifiable = false;
  }
}

class _SelectionAttributsAscendance {
  late List<int> indexBonusAscendance;
  late int indexChoixAttribut;
  List<bool> lstChoixAttributsAscendance = <bool>[
    false,
    false,
    false,
    false,
    false,
    false
  ];
  bool estChoisi = false;

  late List<Widget> ddr;

  _SelectionAttributsAscendance(List<int> lstIndexAttribut) {
    indexBonusAscendance = lstIndexAttribut;
    ddr = <Widget>[
      ListTile(
          selected: lstChoixAttributsAscendance[0],
          onTap: () {
            selectionnerAttribut(0);
          })
    ];
  }

  void selectionnerAttribut(int index) {
    if (lstChoixAttributsAscendance[index]) {
      lstChoixAttributsAscendance[index] = false;
    } else if (!estChoisi) {
      lstChoixAttributsAscendance[index] = true;
    }
  }
}
