import 'package:fiche_de_jeu/src/view/pages/creation_personnage.dart';
import 'package:fiche_de_jeu/src/view/widgets/bouton.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  static const String _nomApp = 'Fiche de jeu - BETA';
  static const String _titreApp = 'Fiche de jeu - Menu Principal';

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: _nomApp,
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: const AffichagePrincipal(titre: _titreApp),
    );
  }
}

class AffichagePrincipal extends StatefulWidget {
  const AffichagePrincipal({super.key, required this.titre});
  final String titre;

  @override
  State<AffichagePrincipal> createState() => _AffichagePrincipalState();
}

class _AffichagePrincipalState extends State<AffichagePrincipal> {
  /*Future<void> readJson() async {
    final String reponse = await rootBundle.loadString('data/classes.json');
    final data = await json.decode(reponse);
    Widget affichage = MenuPrincipal();

    setState(() {});
  }*/

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text(widget.titre)), body: MenuPrincipal());
  }

  // **********PARTIE RÉALISÉ AUTOMATIQUEMENT ! A GARDER AU CAS OU

  /*// This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also a layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headlineMedium,
            ),
            _classes.isNotEmpty? Expanded(
                child: ListView.builder(
                  itemCount: _classes.length,
                    itemBuilder: (context, index) {
                    return Card(
                    child: ListTile(
                    title: Text(_classes[index]["nomClasse"])
                    ),
                    );
    },
                ),
            ): Container()
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: readJson,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }*/
}

class MenuPrincipal extends StatelessWidget {
  final ButtonStyle _buttonStyle = ElevatedButton.styleFrom(
      fixedSize: const Size(242, 40), textStyle: const TextStyle(fontSize: 20));
  MenuPrincipal({super.key});

  Function navCreationPersonnage(BuildContext context) {
    return () => Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => const PageCreationPersonnage()));
  }

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Wrap(
      direction: Axis.vertical,
      spacing: 35,
      children: <Widget>[
        BoutonWidget(
            style: _buttonStyle,
            texteAAfficher: 'Sélectionner un personnage',
            fonction: navCreationPersonnage(context)),
        // ************ CE CODE SERA À COMPLÉTER À L'IMPLÉMENTATION DE NOUVELLES FONCTIONNALITÉS *******************
        /*BoutonWidget(style: _buttonStyle, texteAAfficher: 'Créer un personnage',fonction: ),
              BoutonWidget(style: _buttonStyle, texteAAfficher: 'Modifier un personnage',fonction: ),*/
      ],
    ));
  }
}
